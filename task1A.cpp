#include <iostream>
#include <vector>
#include <string>
 
unsigned int prefix_func(char c, unsigned int previous_prefix, const std::vector<unsigned int> &prefix_array, const std::string &pattern) {
    unsigned int cur = previous_prefix;
    while (cur > 0 and c != pattern[cur]) {
        cur = prefix_array[cur - 1];
    }
    if (c == pattern[cur]) {
        ++cur;
    }
    if (cur == pattern.length() + 1) {
        cur = 0;
    }
    return cur;
}
 
int main() {
    std::string str, pattern;
    std::cin >> pattern >> str;
    std::vector<unsigned int> prefix_array(pattern.length(), 0);
    for (int i = 1; i < pattern.length(); ++i) {
        prefix_array[i] = prefix_func(pattern[i], prefix_array[i - 1], prefix_array, pattern);
    }
    unsigned int previous_prefix = 0;
    for (unsigned int i = 0; i < str.length(); ++i) {
        previous_prefix = prefix_func(str[i], previous_prefix, prefix_array, pattern);
        if (pattern.length() == previous_prefix) {
            std::cout << i - (pattern.length()) + 1 << ' ';
        }
    }
    return 0;
}